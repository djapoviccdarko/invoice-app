@extends('layouts.admin')
@if (session('success'))
    <div class="alert alert-success d-print-none" role="alert">
        {{ session('success') }}
    </div>
@endif
